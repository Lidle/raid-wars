# About
This plugin is an addon to the well-known [SavageFactions](https://www.spigotmc.org/resources/savagefactions-factionsuuid-reimagined-1-7-1-12.52891/) plugin.  This plugin's aim is to provide players with an increased incentive for playing faction servers.  With the demands of the players in mind, this plugin also looks to provide server owners seamless integration between these plugins and to also provide intriguing content within their server's community.

# Description
This plugin allows factions to challenge eachother to what is called "RaidWars".  You create a RaidWars party for your faction and invite fellow members and then challenge other factions to a small minigame.  This allows you to wager items for the duel and the spoils will go to the victor.

The goal is simple, once the game has start the factions must battle eachother to reach the inside of the base.  Once inside, they must locate and capture the center block to win.  The server owners can easily add in new maps on the fly by referencing individual [WorldEdit](https://www.spigotmc.org/resources/fast-async-worldedit-voxelsniper.13932/updates) schematics they wish to use.  Once a game has been created, this will generate a new world with the specified schematic and load the players in before the countdown!

# Images
## Faction Party Interface
![Faction's Party Image](https://i.imgur.com/F2VE0TG.png)