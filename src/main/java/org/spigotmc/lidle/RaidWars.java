package org.spigotmc.lidle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.massivecraft.factions.cmd.FCmdRoot;
import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.MCommand;
import com.massivecraft.factions.zcore.MPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.lidle.commands.RaidWarsCommands;
import org.spigotmc.lidle.commands.subcommands.savagefactions.RaidWarsFactionCommand;
import org.spigotmc.lidle.utils.event.EventListenerRegisterer;
import org.spigotmc.lidle.utils.persist.Persist;

import java.lang.reflect.Modifier;

public class RaidWars extends JavaPlugin {


    private static RaidWars inst;
    private boolean loadSuccessful = false;

    /**
     * Global and final gson instance used around in the plugin
     */
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().
            disableHtmlEscaping().
            excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE, Modifier.STATIC).
            create();

    /**
     * Data file persistence upon reloads/restarts
     * */
    private Persist persist;

    /**
     * Used for logging useful information while debugging
     * */
    private Debug debug;

    @Override
    public void onLoad(){
        // Used to determine if it was a successful plugin enable
        this.loadSuccessful = false;

        inst = this;

        // Ensure base folder exists before doing anything!
        getDataFolder().mkdirs();

        persist = new Persist(this);
    }

    public void onEnable() {
        init();

        this.loadSuccessful = true;
    }

    public void onDisable() {
        if(loadSuccessful) {
            getLogger().info("Plugin successfully disabled!");
        }
    }

    private void init() {
        Debug.isEnabled(true);
        if(Debug.isEnabled()) {
            debug = new Debug(this);
        }

        registerCommands();
        registerConfig();
        registerEvents();
        registerPermissions();

        setupTasks();

        debug.infoSuccess("Plugin enabled!");
    }

    private void registerCommands() {
        getCommand("raidwars").setExecutor(new RaidWarsCommands());
        registerFactionCommandsHook();
    }

    private void registerFactionCommandsHook() {
        FactionsPluginProxy factionsPluginProxy = new FactionsPluginProxy(this);
        factionsPluginProxy.registerCommands();
    }

    private void registerConfig() {
        new PluginConf(this);
    }

    private void registerEvents() {
        EventListenerRegisterer.registerAllEvents();
    }

    private void registerPermissions() {

    }

    private void setupTasks() {

    }

    public boolean hasPermission(CommandSender sender, String perm) {
        if (sender.isOp()) {
            return true;
        }

        if (sender instanceof Player) {
            Player player = (Player) sender;
            return player.hasPermission(perm);
        }

        return false;
    }

    /**
     * Checks to see if the sender is a player, otherwise throw an exception.
     *
     * @param sender The {@link CommandSender} to check
     * @throws CommandException if {@code sender} isn't a {@link Player}
     */
    public boolean checkPlayer(CommandSender sender)
            throws CommandException {
        if (sender instanceof Player) {
            return true;
        } else {
            throw new CommandException("A player is expected.");
        }
    }

    public Persist getPersist() {
        return persist;
    }

    public Debug getDebug() {
        return debug;
    }

    public static RaidWars inst(){
        return inst;
    }


}
