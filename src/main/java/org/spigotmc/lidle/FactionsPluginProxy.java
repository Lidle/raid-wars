package org.spigotmc.lidle;

import com.massivecraft.factions.cmd.FCmdRoot;
import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.MCommand;
import com.massivecraft.factions.zcore.MPlugin;
import org.bukkit.Bukkit;
import org.fusesource.jansi.Ansi;
import org.spigotmc.lidle.commands.subcommands.savagefactions.RaidWarsFactionCommand;

public class FactionsPluginProxy {

    private final RaidWars pl;
    private final MPlugin factionsPl;

    public FactionsPluginProxy(RaidWars pl) {
        this.pl = pl;
        this.factionsPl = getFactionsHook();
    }

    public void registerCommands() {
        FCmdRoot rootCmd = (FCmdRoot) factionsPl.getBaseCommands().get(0);
        FCommand raidwarsCommand = new RaidWarsFactionCommand();
        rootCmd.addSubCommand(raidwarsCommand);
        factionsPl.getBaseCommands().set(0, rootCmd);
    }

    private void registerCommand(MCommand<?> rootCmd, MCommand<?> command) {
        //Registering the root command
        rootCmd.subCommands.add(command);

        //If there are no more subcommands to be registered
        if(command.subCommands != null && command.subCommands.size() > 0) {
            //Registering the root's sub commands and their dependencies recursively
            for(MCommand<?> subcmd : command.subCommands) {
                pl.getLogger().info(subcmd.aliases.get(0));
                if(subcmd != null) {
                    registerCommand(command, subcmd);
                }
            }
        }
    }

    private static MPlugin getFactionsHook() {
        MPlugin mPlugin = (MPlugin) Bukkit.getServer().getPluginManager().getPlugin("Factions");

        if(mPlugin == null) {
            Debug.inst().severe("An error has occurred hooking into the SavageFactions plugin");
        } else {
            Debug.inst().infoSuccess("Hooked into SavageFactions!");
        }

        return mPlugin;
    }

}
