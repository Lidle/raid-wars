package org.spigotmc.lidle.permissions;

import org.bukkit.command.CommandSender;
import org.spigotmc.lidle.RaidWars;

public class FactionsCommandPermissionModel extends AbstractPermissionModel {

    public FactionsCommandPermissionModel(RaidWars pl, CommandSender sender) {
        super(pl, sender);
    }

    public boolean mayUseSavageRaidWars() { return hasPermission(Node.SAVAGE_RAID_WARS); }

    public boolean mayUseWager() {return  hasPermission(Node.WAGER); }

    public boolean mayUseParty() { return hasPermission(Node.PARTY); }

    public boolean mayCreateParty() { return hasPermission(Node.PARTY_CREATE); }

    public boolean mayDisbandParty() { return hasPermission(Node.PARTY_DISBAND); }

    public boolean mayInviteToParty() { return hasPermission(Node.PARTY_INVITE); }

    public boolean mayUsePartyAction() { return hasPermission(Node.PARTY_ACTION); }

    public boolean mayKickFromParty() { return hasPermission(Node.PARTY_KICK); }

    public boolean mayLeaveParty() { return hasPermission(Node.PARTY_LEAVE); }

    public boolean mayUseGame() { return hasPermission(Node.GAME); }

    public static final class Node {
        //Main Permission
        public static final String SAVAGE_RAID_WARS = "commands.factions.raidwar";

        //Wager Permissions
        public static final String WAGER = "commands.factions.wager";

        //Party Permissions
        public static final String PARTY = "commands.factions.party";
        public static final String PARTY_CREATE = PARTY + ".create";
        public static final String PARTY_DISBAND = PARTY + ".disband";
        public static final String PARTY_INVITE = PARTY + ".invite";
        public static final String PARTY_ACTION = PARTY + ".inviteaction";
        public static final String PARTY_KICK = PARTY + ".kick";
        public static final String PARTY_LEAVE = PARTY + ".leave";

        //Game Permissions
        public static final String GAME = "commands.factions.game";
        public static final String GAME_INVITE = GAME + ".invite";
        public static final String GAME_ACTION = GAME + ".inviteaction";
        public static final String GAME_START = GAME + ".start";
        public static final String GAME_LEAVE = GAME + ".leave";
        public static final String GAME_READY = GAME + ".ready";
    }
}
