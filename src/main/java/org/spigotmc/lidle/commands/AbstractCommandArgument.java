package org.spigotmc.lidle.commands;

import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.spigotmc.lidle.RaidWars;
import org.spigotmc.lidle.permissions.CommandPermissionModel;
import org.spigotmc.lidle.utils.messaging.Messages;

import java.util.ArrayList;
import java.util.List;

/*Authored by: Matt Bessler 8/10/2018*/
public abstract class AbstractCommandArgument extends AbstractCommand {

    /**
     * Who invoked this command
     * */
    private CommandSender commandSender;

    /**
     * Permissions required for this command
     * */
    protected CommandPermissionModel permissionModel;

    /**
     * If this command is required to be invoked by a player
     * */
    private boolean requiresPlayer = true;

    /**
     * A list of command arguments associated with it
     * */
    private List<String> commandArguments = new ArrayList<>();

    public AbstractCommandArgument(String cmd) {
        super(cmd);
    }

    public AbstractCommandArgument(String cmd, List<String> aliases) {
        super(cmd, aliases);
    }

    public AbstractCommandArgument(String cmd, String... aliases) {
        super(cmd, aliases);
    }

    protected void setRequiresPlayer(boolean requiresPlayer) {
        this.requiresPlayer = requiresPlayer;
    }

    public void execute(CommandSender commandSender, List<String> paramList) {
        this.commandSender = commandSender;
        this.commandArguments = paramList;
        this.permissionModel = new CommandPermissionModel(RaidWars.inst(), this.commandSender);

        if(canExecute(commandSender)) {
            execute();
        }
    }

    /**
     * @return whether or not the CommandSender can execute this command
     * */
    private boolean canExecute(CommandSender sender) {
        //Checking the sender is a player
        if(requiresPlayer) {
            try {
                RaidWars.inst().checkPlayer(sender);
            } catch (CommandException e) {
                Messages.FormattedMessage.mustBePlayer().sendMessage(sender);
                return false;
            }
        }

        //Checking if the sender has permission
        if(!hasPermission()) {
            Messages.FormattedMessage.noPermission().sendMessage(sender);
            return false;
        }

        return true;
    }

    /**
     * @return the command arguments (if any) passed to this command
     * */
    protected List<String> getCommandArguments() {
        return commandArguments;
    }

    /**
     * @return the sender who executed the command
     * */
    protected CommandSender getCommandSender() {
        return commandSender;
    }

    /**
     * @return the sender casted as a player
     * @throws CommandException if the command argument doesn't require the sender to be a player
     * */
    protected Player getSenderAsPlayer() {
        if(isSenderPlayer()) {
            return (Player) commandSender;
        } else {
            throw new CommandException("Expected a player!");
        }
    }

    protected boolean isSenderPlayer() {
        return commandSender instanceof Player;
    }

    protected abstract boolean hasPermission();

}
