package org.spigotmc.lidle.commands.subcommands.savagefactions.party;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.bukkit.Bukkit;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.messaging.Messages;

public class CreateCommand extends FCommand {

    CreateCommand() {
        super();
        this.aliases.add("create");
        this.permission = FactionsCommandPermissionModel.Node.PARTY_CREATE;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        //Create a new party if they aren't in an existing party
        RaidPlayer raidPlayer = RaidPlayer.getOrCreateRaidPlayer(fme);
        if(Party.canCreateParty(raidPlayer)) {
            PartyEvent event = new PartyEvent(fme, PartyEvent.EventType.CREATE);
            Events.fire(event);
        } else {
            Messages.FormattedMessage
                    .cannotCreateParty()
                    .sendMessage(me);
        }
    }
}
