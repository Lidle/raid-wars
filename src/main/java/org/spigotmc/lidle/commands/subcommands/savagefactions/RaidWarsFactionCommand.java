package org.spigotmc.lidle.commands.subcommands.savagefactions;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.bukkit.Bukkit;
import org.spigotmc.lidle.Debug;
import org.spigotmc.lidle.RaidWars;
import org.spigotmc.lidle.commands.subcommands.savagefactions.game.GameCommand;
import org.spigotmc.lidle.commands.subcommands.savagefactions.game.InviteCommand;
import org.spigotmc.lidle.commands.subcommands.savagefactions.party.PartyCommand;
import org.spigotmc.lidle.commands.subcommands.savagefactions.wager.WagerCommand;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;

public class RaidWarsFactionCommand extends FCommand {

    public RaidWarsFactionCommand() {
        super();
        this.aliases.add("raidwars");
        this.aliases.add("rw");
        this.aliases.add("rws");

        this.addSubCommand(new GameCommand());
        this.addSubCommand(new PartyCommand());
        this.addSubCommand(new WagerCommand());

        this.permission = FactionsCommandPermissionModel.Node.SAVAGE_RAID_WARS;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeModerator = true;
        senderMustBeMember = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {

    }
}
