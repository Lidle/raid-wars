package org.spigotmc.lidle.commands.subcommands.savagefactions;

import org.spigotmc.lidle.commands.AbstractCommandArgument;
import org.spigotmc.lidle.commands.subcommands.AbstractSubCommand;

public abstract class SavageFactionsCommandProxy extends AbstractSubCommand {

    public SavageFactionsCommandProxy(String cmd) {
        super(cmd);
    }

}
