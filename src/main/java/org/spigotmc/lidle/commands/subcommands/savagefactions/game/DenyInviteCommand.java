package org.spigotmc.lidle.commands.subcommands.savagefactions.game;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyInviteActionEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.invites.PartyInvite;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.generics.Action;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class DenyInviteCommand extends FCommand {

    DenyInviteCommand() {
        super();
        this.aliases.add("deny");
        this.aliases.add("decline");

        this.permission = FactionsCommandPermissionModel.Node.PARTY_ACTION;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        RaidPlayer raidPlayer = RaidPlayer.getOrCreateRaidPlayer(fme);

        //Validating that they have an existing invite
        if(!raidPlayer.hasInvite()) {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.INVITE_NO_LONGER_EXISTS)
                    .withMessageType(MessageType.WARNING)
                    .build();

            raidPlayer.sendMessage(message);
            return;
        }

        //Player has a valid invite
        //Fire the PartyInviteAction event!
        PartyInvite invite = raidPlayer.getInvite();

        PartyInviteActionEvent event = new PartyInviteActionEvent(fme, invite, Action.DENY);
        Events.fire(event);
    }
}
