package org.spigotmc.lidle.commands.subcommands.savagefactions.game;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;

public class ReadyCommand extends FCommand {

    ReadyCommand() {
        super();
        this.aliases.add("ready");
        this.aliases.add("begin");

        this.permission = FactionsCommandPermissionModel.Node.GAME_READY;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = true;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {

    }

}
