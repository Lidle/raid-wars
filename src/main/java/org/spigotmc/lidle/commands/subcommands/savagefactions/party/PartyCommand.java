package org.spigotmc.lidle.commands.subcommands.savagefactions.party;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.MCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.Debug;
import org.spigotmc.lidle.commands.subcommands.savagefactions.party.debugcmds.ForceJoinCommand;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;

public class PartyCommand extends FCommand {

    public PartyCommand() {
        super();
        this.aliases.add("party");
        this.permission = FactionsCommandPermissionModel.Node.PARTY;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;

        this.addSubCommand(new CreateCommand());
        this.addSubCommand(new DisbandCommand());
        this.addSubCommand(new InviteCommand());
        this.addSubCommand(new JoinCommand());
        this.addSubCommand(new KickCommand());
        this.addSubCommand(new LeaveCommand());

        if(Debug.isEnabled()) {
            this.addSubCommand(new ForceJoinCommand());
        }
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        if(args.size() == 0) {
            for(MCommand<?> cmd : subCommands) {
                if(cmd.aliases.contains("create")) {
                    commandChain.add(cmd);
                    cmd.execute(sender, args, commandChain);
                    return;
                }
            }
        }
    }
}
