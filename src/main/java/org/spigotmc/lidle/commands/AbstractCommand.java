package org.spigotmc.lidle.commands;

import org.spigotmc.lidle.utils.BukkitUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*Authored by: Matt Bessler 8/11/2018*/
public abstract class AbstractCommand {

    /**
     * The command name
     * */
    private String cmd;

    /**
     * Aliases of this command
     * */
    private List<String> aliases;

    private AbstractCommand(){}

    protected AbstractCommand(String cmd) {
        this(cmd, new ArrayList<>());
    }

    protected AbstractCommand(String cmd, String... aliases) {
        this(cmd, Arrays.asList(aliases));
    }

    protected AbstractCommand(String cmd, List<String> aliases) {
        this.cmd = cmd.toLowerCase();
        this.aliases = BukkitUtil.convertListToLowerCase(aliases);
    }

    public String getCommand() {
        return this.cmd;
    }

    protected abstract String getUsage();

    public List<String> getUsages() {
        return Arrays.asList(getUsage());
    }

    public List<String> getAliases() {
        return this.aliases;
    }

    public boolean isCommand(String cmd) {
        String label = cmd.toLowerCase();
        return this.cmd.equalsIgnoreCase(label) || aliases.contains(label);
    }

    protected abstract boolean execute();

}
