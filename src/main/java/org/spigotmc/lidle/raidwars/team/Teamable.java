package org.spigotmc.lidle.raidwars.team;

import org.spigotmc.lidle.utils.generics.Named;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messageable;

public interface Teamable extends Named, Messageable {

    String getTeamName();

    String getLeaderName();

    default String getName() {
        return getTeamName();
    }

    void notifyMembers(Message msg);

    default void notifyMembers(String msg) {
        Message message = Message.MessageBuilder.builder()
                .withMessage(msg)
                .withMessageType(MessageType.DEFAULT)
                .build();

        notifyMembers(message);
    }

    default void sendMessage(Message message) {
        notifyMembers(message);
    }

}
