package org.spigotmc.lidle.raidwars.invites;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.utils.invites.AbstractInvite;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;

public class PartyInvite extends AbstractInvite<RaidPlayer> {

    private Party invitedParty;

    public PartyInvite(FPlayer fromPlayer, Party invitedParty) {
        this(RaidPlayer.getOrCreateRaidPlayer(fromPlayer),
                invitedParty);
    }

    public PartyInvite(RaidPlayer fromPlayer, Party invitedParty) {
        super(fromPlayer, InviteType.PARTY);
        this.invitedParty = invitedParty;
    }

    public Party getInvitedParty() {
        return invitedParty;
    }

    @Override
    public boolean accept() {
        isAccepted(true);

        if(invitedParty != null && doesExist()) {
            doesExist(false);
            return true;
        }

        return false;
    }

    @Override
    public String getInviteName() {
        return invitedParty.getLeaderName();
    }

    @Override
    public boolean deny() {
        isAccepted(false);
        doesExist(false);

        return isAccepted();
    }

}
