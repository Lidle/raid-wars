package org.spigotmc.lidle.raidwars.party.scoreboard;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Entry {

    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty entry = new SimpleStringProperty();
    private SimpleStringProperty suffix = new SimpleStringProperty();
    private SimpleStringProperty prefix = new SimpleStringProperty();

    public Entry(String name, String entry) {
        this(name, entry, "", "");
    }

    public Entry(String name, String entry, String suffix) {
        this(name, entry, suffix, "");
    }

    public Entry(String name, String entry, String suffix, String prefix) {
        this.name = new SimpleStringProperty(name);
        this.entry = new SimpleStringProperty(entry);
        this.suffix = new SimpleStringProperty(suffix);
        this.prefix = new SimpleStringProperty(prefix);
    }

    public Entry(String name, StringProperty entry, String suffix, String prefix) {
        this.name = new SimpleStringProperty(name);
        this.entry = new SimpleStringProperty();
        this.entry.bind(entry);
        this.suffix = new SimpleStringProperty(suffix);
        this.prefix = new SimpleStringProperty(prefix);
    }

    public String getName() {
        return name.getValue();
    }

    public String getEntry() {
        return entry.getValue();
    }

    public String getSuffix() {
        return suffix.getValue();
    }

    public String getPrefix() {
        return prefix.getValue();
    }

}
