package org.spigotmc.lidle.raidwars.party;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import org.spigotmc.lidle.PluginConf;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.team.Teamable;
import org.spigotmc.lidle.utils.messaging.Message;

import java.util.*;

public class Party extends Observable implements Teamable {

    private static Map<Faction, Party> factionParties = new HashMap<>();
    private static final int maxPartySize = PluginConf.inst().getMaxPartySize();
    private static final int minPartySize = PluginConf.inst().getMinPartySize();

    private Faction faction;
    private RaidPlayer partyLeader;
    private Set<RaidPlayer> partyMembers = new HashSet<>();

    protected Party(Faction faction, RaidPlayer partyLeader) {
        this.faction = faction;
        this.partyLeader = partyLeader;
        registerNewParty();
    }

    protected Party(Party other) {
        this.faction = other.faction;
        this.partyLeader = other.partyLeader;
        this.partyMembers = other.partyMembers;
    }

    /**
     * Initializes a new party for a faction and adds a new {@link PartyScoreboard} observer to be
     * notified of any party changes so it can update the scoreboard display accordingly
     * */
    private void registerNewParty() {
        partyMembers.add(partyLeader);
        factionParties.put(faction, this);
        partyLeader.setParty(this);
        addObserver(new PartyScoreboard(this));
    }

    /**
     * @return the leader of this party
     * */
    public RaidPlayer getPartyLeader() {
        return partyLeader;
    }

    /**
     * Adds player to the party's party members and notifies its observers
     * @return true if player successfully joined party | false if
     * their isn't enough space in the party
     * */
    public boolean addPartyMember(RaidPlayer raidPlayer) {
        if(isPartyFull() && partyMembers.add(raidPlayer)) {
            raidPlayer.setParty(this);
            setChanged();
            notifyObservers(raidPlayer);
            return true;
        } else {
            //Not enough space in the party
            return false;
        }
    }

    /**
     * Removes player from the party's party members and notifies its observers
     * @return true if the player was successfully removed from the party; false
     * if the player could not be found
     * */
    public boolean removePartyMember(RaidPlayer raidPlayer) {
        if(partyMembers.remove(raidPlayer)) {
            //Player was successfully removed from the party
            raidPlayer.setParty(null);
            setChanged();
            notifyObservers(raidPlayer);
            return true;
        } else {
            //Player was not in the party
            return false;
        }
    }

    /**
     * <b>This should be used for debugging online!</b>
     * <br><br>
     * Forces the party to add a new member and notifies its observers
     * @return true always
     * */
    public boolean forceAddPartyMember(RaidPlayer raidPlayer) {
        partyMembers.add(raidPlayer);
        setChanged();
        notifyObservers(raidPlayer);

        return true;
    }

    /**
     * @return true if the specified player is the party leader for this party | false otherwise
     * */
    public boolean isPartyLeader(FPlayer player) {
        UUID uuid = UUID.fromString(player.getId());
        UUID partyLeaderUUID = partyLeader.getUUID();
        return uuid.equals(partyLeaderUUID);
    }

    /**
     * @return true if the specified player is a member of this party | false otherwise
     * */
    public boolean isPartyMember(FPlayer player) {
        UUID uuid = UUID.fromString(player.getId());
        for(RaidPlayer partyMember : partyMembers) {
            UUID partyMemberUUID = partyMember.getUUID();
            if(uuid.equals(partyMemberUUID)) {
                return true;
            }
        }

        return false;
    }

    /**
     * The faction players currently in this party
     * */
    public Set<RaidPlayer> getPartyMembers() {
        return partyMembers;
    }

    /**
     * @return true if the party has reached its capacity | false otherwise
     * */
    private boolean isPartyFull() {
        return partyMembers.size() + 1 <= maxPartySize;
    }

    /**
     * Disbands the current party for this faction
     * */
    public void disband() {
        factionParties.remove(faction);
        this.partyMembers.clear();
        this.partyMembers = null;
        this.partyLeader = null;
        setChanged();
        notifyObservers(this);
    }

    /**
     * Sends a {@link Message} to each party member
     * */
    @Override
    public void notifyMembers(Message message) {
        partyMembers.forEach(partyMember -> {
            partyMember.sendMessage(message);
        });
    }

    /**
     * @return the faction name for this party
     * */
    @Override
    public String getTeamName() {
        return faction.getTag();
    }

    /**
     * @return the display name of the party leader
     * */
    @Override
    public String getLeaderName() {
        return partyLeader.getPlayer().getDisplayName();
    }

    @Override
    public void sendTitle(Message title, Message subTitle, int fadeIn, int stay, int fadeOut) {
        partyMembers.forEach(member -> {
            member.sendTitle(title, subTitle, fadeIn, stay, fadeOut);
        });
    }

    /**
     * Registers a new party for the specified faction
     * @return the new party registered
     * */
    public static Party registerNewParty(Faction faction, RaidPlayer partyLeader) {
        return new Party(faction, partyLeader);
    }

    /**
     * @return the {@link Party} if one currently exists for the faction | null otherwise
     * */
    public static Party getPartyForFaction(Faction faction) {
        return factionParties.get(faction);
    }

    /**
     * @return true if the player isn't in a party or if a party already exists for their
     * faction | false otherwise
     * */
    public static boolean canCreateParty(FPlayer raidPlayer) {
        Faction faction = raidPlayer.getFaction();

        boolean inParty = inParty(raidPlayer);
        boolean doesPartyExistForFaction = doesPartyExistForFaction(faction);
        return !inParty && !doesPartyExistForFaction;
    }

    /**
     * @return true if the player is in a party | false otherwise
     * */
    public static boolean inParty(FPlayer player) {
        Faction faction = player.getFaction();
        RaidPlayer raidPlayer = RaidPlayer.getOrCreateRaidPlayer(player);
        if(factionParties.containsKey(faction)) {
            Party party = factionParties.get(faction);
            return party.isPartyMember(raidPlayer);
        }
        return false;
    }

    /**
     * @return true if a party exists for the current faction | false otherwise
     * */
    public static boolean doesPartyExistForFaction(Faction faction) {
        return factionParties.containsKey(faction);
    }

}
