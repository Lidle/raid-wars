package org.spigotmc.lidle.raidwars.party;

import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyEvent;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyListener;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.utils.generics.PartyActionNotificationDispatcher;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messageable;
import org.spigotmc.lidle.utils.messaging.Messages;

/**
 * Handles performing notifications based on actions invoked on the {@link Party} object
 * by the {@link PartyEvent}'s {@link PartyListener} event handler
 * */
public class PartyNotificationDispatcher implements PartyActionNotificationDispatcher {

    private Party party;
    private RaidPlayer whoInvoked;
    private RaidPlayer invokedOn;

    public PartyNotificationDispatcher(Party party, RaidPlayer whoInvoked) {
        this.party = party;
        this.whoInvoked = whoInvoked;
        this.invokedOn = null;
    }

    public PartyNotificationDispatcher(Party party, RaidPlayer whoInvoked, RaidPlayer invokedOn) {
        this.party = party;
        this.whoInvoked = whoInvoked;
        this.invokedOn = invokedOn;
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public void dispatchPlayerJoinedPartyAction() {
        Message message = Messages.FormattedMessage.playerJoinedParty(whoInvoked.getPlayer());
        dispatchPartyNotification(message);
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public void dispatchPlayerLeftPartyAction() {
        //Notify leaving player they have left the party
        Message youHaveLeftPartyNotification = Message.MessageBuilder.builder()
                .withMessage(Messages.YOU_HAVE_LEFT_THE_PARTY)
                .withMessageType(MessageType.WARNING)
                .build();
        dispatchNotificationToWhoInvoked(youHaveLeftPartyNotification);

        //Notifying party that the player has left the party
        String msg = Messages.PLAYER_HAS_LEFT_PARTY;
        String leavingPlayerName = whoInvoked.getPlayer().getName();
        msg = msg.replaceAll("%player%", leavingPlayerName);
        Message playerLeftPartyNotification = Message.MessageBuilder.builder()
                .withMessage(msg)
                .withMessageType(MessageType.WARNING)
                .build();
        dispatchPartyNotification(playerLeftPartyNotification);
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public void dispatchKickMemberAction() {
        RaidPlayer partyLeader = party.getPartyLeader();

        //Notifying party leader they have kicked the player
        Message partyLeaderNotification = Messages.FormattedMessage
                .youRemovedPlayerFromParty(whoInvoked.getPlayer());

        //Notify kicked player they have been kicked
        Message kickedPlayerNotification = Messages.FormattedMessage
                .youWereKickedFromPartyBy(partyLeader.getPlayer());

        //Notify party members that player has been kicked
        Message partyNotification = Messages.FormattedMessage
                .playerKickedFromParty(partyLeader.getPlayer(), invokedOn.getPlayer());

        dispatchPartyLeaderNotification(partyLeaderNotification);
        dispatchNotificationToMemberInvokedOn(kickedPlayerNotification, invokedOn);
        dispatchPartyNotification(partyNotification);
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public void dispatchDisbandPartyAction() {
        Message message = Message.MessageBuilder.builder()
                .withMessage(Messages.DISBANDED_PARTY)
                .withMessageType(MessageType.SUCCESS)
                .build();

        dispatchPartyNotification(message);
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public void dispatchPartyLeaderNotification(Message message) {
        RaidPlayer partyLeader = party.getPartyLeader();
        partyLeader.sendMessage(message);
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public void dispatchPartyNotification(Message message) {
        party.notifyMembers(message);
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public void dispatchNotificationToWhoInvoked(Message message) {
        whoInvoked.sendMessage(message);
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public void dispatchNotificationToMemberInvokedOn(Message message, Messageable who) {
        who.sendMessage(message);
    }

}
