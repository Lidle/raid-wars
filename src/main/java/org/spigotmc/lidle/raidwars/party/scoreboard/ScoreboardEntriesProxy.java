package org.spigotmc.lidle.raidwars.party.scoreboard;

import org.spigotmc.lidle.raidwars.players.RaidPlayer;

import java.util.HashSet;
import java.util.Set;

public class ScoreboardEntriesProxy {

    // TODO: 8/22/2018 implement this to be used inside PartyScoreboard

    private ScoreboardEntries entries;
    private Set<RaidPlayer> activeScoreboards = new HashSet<>();

    public ScoreboardEntriesProxy(ScoreboardEntries entries) {
        this.entries = entries;
    }

}
