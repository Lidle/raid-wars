package org.spigotmc.lidle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Attribute;

public final class Debug {

    private static boolean enabled = false;

    private Plugin pl;
    private ColouredConsoleSender sender;

    Debug(Plugin pl) {
        this.pl = pl;
        this.sender = new ColouredConsoleSender();
    }

    public void info(String msg) {
        msg = sender.getMessage(ChatColor.AQUA + msg);
        pl.getLogger().info(msg);
    }

    public void infoSuccess(String msg) {
        msg = sender.getMessage(ChatColor.GREEN + msg);
        pl.getLogger().info(msg);
    }

    public void warning(String msg) {
        msg = sender.getMessage(ChatColor.YELLOW + msg);
        pl.getLogger().info(msg);
    }

    public void severe(String msg) {
        msg = sender.getMessage(ChatColor.RED + msg);
        pl.getLogger().info(msg);
    }

    public void broadcastMessage(String message) {
        Bukkit.broadcastMessage(message);
    }

    private String formatMsg(String msg) {
        return msg + Ansi.ansi().a(Ansi.Attribute.RESET);
    }

    public static boolean isEnabled(boolean isEnabled) {
        enabled = isEnabled;
        return isEnabled();
    }

    public static boolean isEnabled() {
        return enabled;
    }

    public static Debug inst() {
        return RaidWars.inst().getDebug();
    }

}
