package org.spigotmc.lidle.events.subevents.savagefactions.party;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.event.FactionDisbandEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.spigotmc.lidle.RaidWars;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyEvent.EventType;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.party.PartyNotificationDispatcher;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.utils.event.EventListenerRegisterer;
import org.spigotmc.lidle.utils.generics.Action;
import org.spigotmc.lidle.utils.invites.InviteDispatcher;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class PartyListener implements Listener {

    public PartyListener() {
        EventListenerRegisterer.addListener(this);
    }

    @EventHandler
    private void onPlayerLeave(PlayerQuitEvent e) {
        FPlayer fPlayer = FPlayers.getInstance().getByPlayer(e.getPlayer());
        RaidPlayer raidPlayer = RaidPlayer.getOrCreateRaidPlayer(fPlayer);
        Faction faction = fPlayer.getFaction();

        if(Party.inParty(fPlayer)) {
            Party party = Party.getPartyForFaction(faction);
            if(party.isPartyLeader(raidPlayer)) {
                party.disband();
            } else {
                party.removePartyMember(raidPlayer);
            }
        }
    }


    @EventHandler
    private void onFactionDisband(FactionDisbandEvent e) {
        //Disbanding the faction's party if they disband their faction
        Faction faction = e.getFaction();

        if(Party.doesPartyExistForFaction(faction)) {
            Party party = Party.getPartyForFaction(faction);
            party.disband();
        }
    }

    @EventHandler
    public void onPartyEvent(PartyEvent e) {
        EventType eventType = e.getEventType();
        switch (eventType) {
            case CREATE:
                create(e);
                break;
            case DISBAND:
                disband(e);
                break;
            case INVITE:
                invite((PartyInviteEvent) e);
                break;
            case INVITE_ACTION:
                inviteAction((PartyInviteActionEvent) e);
                break;
            case KICK:
                kick((PartyKickEvent) e);
                break;
            case LEAVE:
                leave(e);
                break;
            default:
                break;
        }
    }

    private void create(PartyEvent e) {
        RaidPlayer player = e.getPlayer();
        Faction faction = player.getFaction();

        RaidPlayer partyLeader = RaidPlayer.getOrCreateRaidPlayer(player);

        Party.registerNewParty(faction, partyLeader);

        String msg = Messages.PARTY_CREATED;
        String factionTag = ChatColor.GOLD + faction.getTag() + ChatColor.RESET;
        msg = msg.replaceAll("%faction%", factionTag);
        Message message = Message.MessageBuilder.builder()
                .withMessage(msg)
                .withMessageType(MessageType.SUCCESS)
                .build();

        message.sendMessage(player.getPlayer());
    }

    private void disband(PartyEvent e) {
        //Notifying the party members their party is being disbanded
        PartyNotificationDispatcher notificationDispatcher = e.getPartyNotificationDispatcher();
        notificationDispatcher.dispatchDisbandPartyAction();

        //Disbaned the party
        Party party = e.getParty();
        party.disband();
    }

    private void invite(PartyInviteEvent e) {
        //Getting the name of the player who invited them
        RaidPlayer whoInvited = e.getPlayer();
        String playerName = ChatColor.GOLD + whoInvited.getName() + ChatColor.RESET;

        //Message notification to tell the player they have been invited to a party
        String msg = Messages.YOU_HAVE_BEEN_INVITED_TO_PARTY;
        msg = msg.replaceAll("%player%", playerName);
        Message message = Message.MessageBuilder.builder()
                .withMessage(msg)
                .withMessageType(MessageType.SUCCESS)
                .build();

        //Dispatching the invite to the player
        InviteDispatcher dispatcher = e.getInviteDispatcher();
        dispatcher.dispatchInviteRequest(message);
    }

    private void inviteAction(PartyInviteActionEvent e) {
        //Getting the invite action
        Action action = e.getAction();

        //Getting the name of the player who invoked the action on the invite
        RaidPlayer raidPlayer = e.getPlayer();
        String playerName = raidPlayer.getName();

        //The dispatchers to handle actions and notifications
        InviteDispatcher inviteDispatcher = e.getInviteDispatcher();
        PartyNotificationDispatcher partyNotificationDispatcher = e.getPartyNotificationDispatcher();

        if(action == Action.ACCEPT) {
            String msg = Messages.INVITED_PLAYER_ACCEPTED_PARTY_INVITE;
            msg = msg.replaceAll("%player%", playerName);
            Message message = Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.SUCCESS)
                    .build();

            inviteDispatcher.dispatchInviteAccepted(message);
            partyNotificationDispatcher.dispatchPlayerJoinedPartyAction();
        } else if(action == Action.DENY) {
            String msg = Messages.INVITED_PLAYER_DENIED_PARTY_INVITE;
            msg = msg.replaceAll("%player%", playerName);
            Message message = Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.WARNING)
                    .build();

            inviteDispatcher.dispatchInviteDenied(message);
        }
    }

    private void kick(PartyKickEvent e) {
        PartyNotificationDispatcher notificationDispatcher = e.getPartyNotificationDispatcher();

        Party party = e.getParty();
        RaidPlayer playerToKick = e.getPlayerToKick();

        //If the player was successfully removed from the party, send message to respective players
        if(party.removePartyMember(playerToKick)) {
            notificationDispatcher.dispatchKickMemberAction();
        } else {
            //Player was not in the party
            Player player = playerToKick.getPlayer();
            String msg = Messages.PLAYER_NOT_IN_PARTY.replaceAll("%player%", player.getName());

            //Notify party leader that player could not be kicked
            Message message = Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.WARNING)
                    .build();

            notificationDispatcher.dispatchPartyLeaderNotification(message);
        }

    }

    private void leave(PartyEvent e){
        Party party = e.getParty();
        RaidPlayer leavingPlayer = RaidPlayer.getOrCreateRaidPlayer(e.getPlayer());
        PartyNotificationDispatcher partyNotificationDispatcher = e.getPartyNotificationDispatcher();

        //If the player was removed successfully, notify the proper groups
        if(party.removePartyMember(leavingPlayer)) {
            partyNotificationDispatcher.dispatchPlayerLeftPartyAction();
        } else {
            //The player was not in a party
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.NOT_IN_PARTY)
                    .withMessageType(MessageType.WARNING)
                    .build();
            message.sendMessage(leavingPlayer.getPlayer());
        }
    }




}
