package org.spigotmc.lidle.events.subevents.savagefactions.party;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.events.subevents.savagefactions.FactionsEvent;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.party.PartyNotificationDispatcher;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;

public class PartyEvent extends FactionsEvent {

    private EventType eventType;
    private Party party;
    private PartyNotificationDispatcher partyDispatcher;

    public PartyEvent(FPlayer whoFired, EventType eventType) {
        super(whoFired);
        this.eventType = eventType;
        this.party = Party.getPartyForFaction(whoFired.getFaction());
        this.partyDispatcher = new PartyNotificationDispatcher(party, getPlayer());
    }

    protected void setPartyNotificationDispatcher(PartyNotificationDispatcher partyDispatcher) {
        this.partyDispatcher = partyDispatcher;
    }

    /**
     * @return the party dispatcher to notify the respective members of the party which action has occurred
     * */
    public PartyNotificationDispatcher getPartyNotificationDispatcher() {
        return partyDispatcher;
    }

    public Party getParty() {
        return party;
    }

    public EventType getEventType() {
        return eventType;
    }

    public enum EventType {
        CREATE,
        DISBAND,
        INVITE,
        INVITE_ACTION,
        KICK,
        LEAVE;
    }

}
