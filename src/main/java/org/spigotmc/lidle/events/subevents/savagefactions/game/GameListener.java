package org.spigotmc.lidle.events.subevents.savagefactions.game;

import com.massivecraft.factions.Faction;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.invites.GameInvite;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.tasks.game.GameStartTask;
import org.spigotmc.lidle.utils.generics.Action;
import org.spigotmc.lidle.utils.event.EventListenerRegisterer;
import org.spigotmc.lidle.utils.invites.InviteDispatcher;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class GameListener implements Listener {

    public GameListener() {
        EventListenerRegisterer.addListener(this);
    }

    @EventHandler
    private void onGameEvent(GameEvent e) {
        GameEvent.EventType eventType = e.getEventType();

        switch (eventType) {
            case LEAVE:
                leave(e);
                break;
            case INVITE:
                invite((GameInviteEvent) e);
                break;
            case INVITE_ACTION:
                inviteAction((GameInviteActionEvent) e);
                break;
            case START:
                start((GameStartEvent) e);
                break;
            default:
                break;
        }

    }

    /**
     * Handler for when a team leaves a game early
     * */
    private void leave(GameEvent e) {
    }

    /**
     * Handler for when a team is invited to join a game
     * */
    private void invite(GameInviteEvent e){
        //Dispatch the invite to the appropriate Team which was invited
        Team teamWhoInvited = e.getTeamWhoInvited();
        Game invitedGame = e.getInvitedGame();
        GameInvite gameInvite = new GameInvite(teamWhoInvited, invitedGame);

        Team teamInvited = e.getInvitedTeam();

        InviteDispatcher dispatcher = new InviteDispatcher(gameInvite, teamInvited);
        dispatcher.dispatchInviteRequest();
    }

    /**
     * Handler for when a team invokes an action on their game invite
     * */
    private void inviteAction(GameInviteActionEvent e) {
        Action action = e.getAction();
        InviteDispatcher inviteDispatcher = e.getInviteDispatcher();
        RaidPlayer playerWhoAccepted = e.getPlayer();
        Faction faction = e.getPlayer().getFaction();
        String factionTag = faction.getTag();

        if(action == Action.ACCEPT) {
            String msg = Messages.INVITED_TEAM_ACCEPTED_GAME_INVITE;
            msg = msg.replaceAll("%faction%", factionTag);
            Message message = Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.SUCCESS)
                    .build();

            //Accepts their invite and notifies both parties
            inviteDispatcher.dispatchInviteAccepted(message);
        } else if (action == Action.DENY) {
            String msg = Messages.INVITED_TEAM_DENIED_GAME_INVITE;
            msg = msg.replaceAll("%faction%", factionTag);
            Message message = Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.WARNING)
                    .build();

            //Denies their invite and notifies both parties
            inviteDispatcher.dispatchInviteDenied(message);
        } else {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.INVALID_ARGUMENTS)
                    .withMessageType(MessageType.WARNING)
                    .build();

            playerWhoAccepted.sendMessage(message);
        }

    }

    /**
     * Handler for when a team requests to start a game
     * */
    private void start(GameStartEvent e) {
        long countdown = e.getCountdownSeconds();
        Game game = e.getGame();

        GameStartTask task = new GameStartTask(game, countdown);
        task.executeTask();
    }

    /**
     * Handler for when a team is ready to start a game
     * */
    private void ready(GameEvent e) {

    }
}
