package org.spigotmc.lidle.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RaidWarsEvent extends Event implements Cancellable {

    protected Player whoFired;

    private boolean isCancelled = false;

    protected RaidWarsEvent(Player whoFired) {
        this.whoFired = whoFired;
    }

    public Player getPlayer() {
        return this.whoFired;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    /*Boiler plate code required for custom events*/
    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
