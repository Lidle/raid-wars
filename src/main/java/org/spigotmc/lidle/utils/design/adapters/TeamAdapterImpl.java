package org.spigotmc.lidle.utils.design.adapters;

import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.team.Team;

import java.util.Set;
import java.util.UUID;

public class TeamAdapterImpl extends Team implements TeamAdapter {

    public TeamAdapterImpl(Team team) {
        super(team);
    }

    /**
     * @return the current {@link Game} the party is queued in if it exists | null otherwise
     * */
    @Override
    Game getGameForTeam();

    /**
     * @return the current members for this team
     * */
    @Override
    Set<RaidPlayer> getTeamMembers();

    /**
     * @return the number of members this team has
     * */
    @Override
    int teamSize();

    /**
     * @return the faction tag for this team
     * */
    @Override
    String getTeamName();

    /**
     * @return the {@link UUID} of the team leader
     * */
    @Override
    UUID getTeamLeaderUUID();

    /**
     * @return the display name of the team leader
     * */
    @Override
    String getLeaderName();
}
