package org.spigotmc.lidle.utils.design.adapters;

import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.utils.generics.State;

public class GameAdapterImpl extends Game implements GameAdapter {

    public GameAdapterImpl(Game game) {
        super(game);
    }

    @Override
    public Team getTeamForParty() {
        return null;
    }

    @Override
    public Team getOpposingTeamFor(Team team) {
        return null;
    }

    @Override
    public State getStartGameState() {
        return null;
    }

    @Override
    public boolean doesTeamHaveEnoughMembers() {
        return false;
    }
}
