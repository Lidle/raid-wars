package org.spigotmc.lidle.utils.design.adapters;

import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;

import java.util.Set;
import java.util.UUID;

interface TeamAdapter {

    /**
     * @return the current {@link Game} the party is queued in if it exists | null otherwise
     * */
    Game getGameForTeam();

    /**
     * @return the current members for this team
     * */
    Set<RaidPlayer> getTeamMembers();

    /**
     * @return the number of members this team has
     * */
    int teamSize();

    /**
     * @return the faction tag for this team
     * */
    String getTeamName();

    /**
     * @return the {@link UUID} of the team leader
     * */
    UUID getTeamLeaderUUID();

    /**
     * @return the display name of the team leader
     * */
    String getLeaderName();

}
