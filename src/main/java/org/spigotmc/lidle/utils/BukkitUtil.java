package org.spigotmc.lidle.utils;

import com.google.common.collect.ImmutableList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*Authored by: Matt Bessler 7/20/2018*/
public class BukkitUtil {

    private static Method ONLINE_PLAYERS_METHOD;

    private BukkitUtil()  {

    }


    /**
     * Checks if the given potion is a vial of water.
     *
     * @param item the item to check
     * @return true if it's a water vial
     */
    public static boolean isWaterPotion(ItemStack item) {
        return (item.getDurability() & 0x3F) == 0;
    }

    /**
     * Get just the potion effect bits. This is to work around bugs with potion
     * parsing.
     *
     * @param item item
     * @return new bits
     */
    public static int getPotionEffectBits(ItemStack item) {
        return item.getDurability() & 0x3F;
    }

    /**
     * Replace color macros in a string. The macros are in the form of `[char]
     * where char represents the color. R is for red, Y is for yellow,
     * G is for green, C is for cyan, B is for blue, and P is for purple.
     * The uppercase versions of those are the darker shades, while the
     * lowercase versions are the lighter shades. For white, it's 'w', and
     * 0-2 are black, dark grey, and grey, respectively.
     *
     * @param str
     * @return color-coded string
     */
    public static String replaceColorMacros(String str) {
        str = str.replace("&r", ChatColor.RED.toString());
        str = str.replace("&R", ChatColor.DARK_RED.toString());

        str = str.replace("&y", ChatColor.YELLOW.toString());
        str = str.replace("&Y", ChatColor.GOLD.toString());

        str = str.replace("&g", ChatColor.GREEN.toString());
        str = str.replace("&G", ChatColor.DARK_GREEN.toString());

        str = str.replace("&c", ChatColor.AQUA.toString());
        str = str.replace("&C", ChatColor.DARK_AQUA.toString());

        str = str.replace("&b", ChatColor.BLUE.toString());
        str = str.replace("&B", ChatColor.DARK_BLUE.toString());

        str = str.replace("&p", ChatColor.LIGHT_PURPLE.toString());
        str = str.replace("&P", ChatColor.DARK_PURPLE.toString());

        str = str.replace("&0", ChatColor.BLACK.toString());
        str = str.replace("&1", ChatColor.DARK_GRAY.toString());
        str = str.replace("&2", ChatColor.GRAY.toString());
        str = str.replace("&w", ChatColor.WHITE.toString());

        str = str.replace("&k", ChatColor.MAGIC.toString());
        str = str.replace("&l", ChatColor.BOLD.toString());
        str = str.replace("&m", ChatColor.STRIKETHROUGH.toString());
        str = str.replace("&n", ChatColor.UNDERLINE.toString());
        str = str.replace("&o", ChatColor.ITALIC.toString());

        str = str.replace("&x", ChatColor.RESET.toString());

        return str;
    }

    public static String coloredMessage(ChatColor color, String str) {
        return color + str + ChatColor.RESET;
    }

    /**
     * Search an enum for a value, and return the first one found. Return null if the
     * enum entry is not found.
     *
     * @param enumType enum class
     * @param values values to test
     * @return a value in the enum or null
     */
    @Deprecated
    public static <T extends Enum<T>> T tryEnum(Class<T> enumType, String ... values) {
        for (String val : values) {
            try {
                return Enum.valueOf(enumType, val);
            } catch (IllegalArgumentException e) {
            }
        }

        return null;
    }

    /**
     * Get a collection of the currently online players.
     *
     * @return The online players
     */
    @SuppressWarnings("unchecked")
    public static Collection<? extends Player> getOnlinePlayers() {
        try {
            return Bukkit.getServer().getOnlinePlayers();
        } catch (NoSuchMethodError ignored) {
        }

        try {
            if (ONLINE_PLAYERS_METHOD == null) {
                ONLINE_PLAYERS_METHOD = getOnlinePlayersMethod();
            }

            Object result = ONLINE_PLAYERS_METHOD.invoke(Bukkit.getServer());
            if (result instanceof Player[]) {
                return ImmutableList.copyOf((Player[]) result);
            } else if (result instanceof Collection<?>) {
                return (Collection<? extends Player>) result;
            } else {
                throw new RuntimeException("Result of getOnlinePlayers() call was not a known data type");
            }
        } catch (Exception e) {
            throw new RuntimeException("WorldGuard is not compatible with this version of Bukkit", e);
        }
    }

    private static Method getOnlinePlayersMethod() throws NoSuchMethodException {
        try {
            return Server.class.getMethod("getOnlinePlayers");
        } catch (NoSuchMethodException e1) {
            return Server.class.getMethod("_INVALID_getOnlinePlayers");
        }
    }

    public static String formatStringListToString(List<String> list) {
        return formatStringListToString(list, true);
    }

    public static String formatStringListToString(List<String> list, boolean appendSpaces) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < list.size(); i++) {
            String word = list.get(i);
            boolean appendSpace = (!isLastElement(list, i) && appendSpaces);
            builder.append(word)
                    .append(appendSpace ? " " : "");
        }

        return builder.toString();
    }

    private static boolean isLastElement(List<?> list, int index) {
        return index == list.size() - 1;
    }

    private static boolean isWordNewLoreLineCharacter(String word) {
        return word.equalsIgnoreCase("|");
    }

    public static List<String> convertListToLowerCase(List<String> list) {
        List<String> tmpList = new ArrayList<>(list);
        tmpList.forEach(element -> {
            element = element.toLowerCase();
        });

        return tmpList;
    }

    /**
     * Tries to parse an integer, if failed returns false
     *
     * @param str String to try to parse
     * @return True if string is an integer
     */
    public static boolean isNumber(String str) {
        try {
            int i = Integer.parseInt(str);
        } catch (NumberFormatException notNumber) {
            return false;
        }
        return true;
    }

}

