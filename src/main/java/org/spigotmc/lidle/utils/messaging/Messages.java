package org.spigotmc.lidle.utils.messaging;

import com.massivecraft.factions.Faction;
import org.bukkit.entity.Player;

/*Authored by: mbess 6/30/2018*/
public class Messages {

    public static final String ALREADY_IN_PARTY = "You are already in a party";
    public static final String CANNOT_CREATE_PARTY = "You cannot create a party for your faction";
    public static final String DISBANDED_PARTY = "You have disbanded your faction's party";
    public static final String GAME_START_CONDITION_NOT_MET = "There is no game ready to be started or you have no opponent";
    public static final String GENERIC_ACCEPTED_INVITE = "Your invite has been accepted";
    public static final String GENERIC_DENIED_INVITE = "Your invite has been denied";
    public static final String GENERIC_INVITE_RECEIVED = "You have received an invite";
    public static final String INVALID_ARGUMENTS_ENTERED = "You entered invalid arguments";
    public static final String INVITED_PLAYER_ACCEPTED_PARTY_INVITE = "%player% has accepted party invite";
    public static final String INVITED_PLAYER_DENIED_PARTY_INVITE = "%player% has denied the party invite";
    public static final String INVITED_TEAM_ACCEPTED_GAME_INVITE = "%faction% has accepted the game invite, prepare";
    public static final String INVITED_TEAM_DENIED_GAME_INVITE = "%faction% has denied the game invite";
    public static final String INVALID_ARGUMENTS = "You entered invalid arguments";
    public static final String INVITE_NO_LONGER_EXISTS = "This invite no longer exists";
    public static final String ITEM_IS_NULL = "Item is null";
    public static final String MUST_BE_PLAYER = "You must be a player to use this command";
    public static final String NO_PERMISSION = "You do not have permission to use this command";
    public static final String NOT_ENOUGH_ARGUMENTS = "You entered too few arguments";
    public static final String NOT_ENOUGH_SPACE_IN_PARTY = "There is not enough space in the party";
    public static final String NOT_IN_PARTY = "You are not in a party";
    public static final String NOT_PARTY_LEADER = "You are not the party leader";
    public static final String PARTY_CREATED = "You have created a new party for your faction: %faction%";
    public static final String PARTY_DOESNT_EXIST = "A party doesn't exist for your faction";
    public static final String PARTY_INVITE_DOESNT_EXIST = "This party invite doesn't exist";
    public static final String PLAYER_IN_PARTY = "That player is already in a party";
    public static final String PLAYER_JOINED_PARTY = "%player% has joined the party";
    public static final String PLAYER_KICKED_FROM_PARTY = "%party leader% kicked %player% from the party";
    public static final String PLAYER_HAS_LEFT_PARTY = "%player% has left the party";
    public static final String PLAYER_NOT_FOUND = "The player %player% could not be found";
    public static final String PLAYER_NOT_IN_SAME_FACTION = "That player isn't in the same faction";
    public static final String PLAYER_NOT_IN_PARTY = "The specified player %player% isn't in your party";
    public static final String PLAYER_OFFLINE = "That player is offline";
    public static final String TEAM_DOESNT_EXIST = "No current team for a game exists for your party";
    public static final String YOUR_PARTY_ACCEPTED_GAME_INVITE = "Your party accepted a game invite against %faction%, prepare";
    public static final String YOUR_PARTY_DENIED_GAME_INVITE = "Your party denied a game invite against %faction%";
    public static final String YOUR_PARTY_WAS_INVITED_TO_GAME = "You have been invited to a game by the faction %faction%";
    public static final String YOU_HAVE_BEEN_INVITED_TO_PARTY = "%player% has invited you to a party";
    public static final String YOU_HAVE_LEFT_THE_PARTY = "You have left the party";
    public static final String YOU_REMOVED_PLAYER_FROM_PARTY = "%player% was removed from the party";
    public static final String YOU_WERE_KICKED_FROM_PARTY_BY = "%party leader% has kicked you from their party";

    public static String recursive(int numOfTimes, String str1, String str2){
        if (numOfTimes - 1 == 0)
            return str1 + str2;

        return recursive(numOfTimes - 1, str1 + str2, str2);
    }

    public static final class FormattedMessage {

        public static Message cannotCreateParty() {
            return Message.MessageBuilder.builder()
                    .withMessage(CANNOT_CREATE_PARTY)
                    .withMessageType(MessageType.WARNING)
                    .build();
        }

        public static Message itemIsNull() {
            return Message.MessageBuilder.builder()
                    .withMessage(ITEM_IS_NULL)
                    .withMessageType(MessageType.ERROR)
                    .build();
        }

        public static Message mustBePlayer() {
            return Message.MessageBuilder.builder()
                    .withMessage(MUST_BE_PLAYER)
                    .withMessageType(MessageType.ERROR)
                    .build();
        }

        public static Message noPermission() {
            return Message.MessageBuilder.builder()
                    .withMessage(NO_PERMISSION)
                    .withMessageType(MessageType.WARNING)
                    .build();
        }

        public static Message notEnoughSpaceInParty() {
            return Message.MessageBuilder.builder()
                    .withMessage(NOT_ENOUGH_SPACE_IN_PARTY)
                    .withMessageType(MessageType.WARNING)
                    .build();
        }

        public static Message notInParty(Player player) {
            String msg = PLAYER_NOT_IN_PARTY.replaceAll("%player%", player.getName());
            return Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.WARNING)
                    .build();
        }

        public static Message playerKickedFromParty(Player partyLeader, Player kickedPlayer) {
            String msg = PLAYER_KICKED_FROM_PARTY;
            msg = msg.replaceAll("%party leader%", partyLeader.getName());
            msg = msg.replaceAll("%player%", kickedPlayer.getName());

            return Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.INFO)
                    .build();
        }

        public static Message playerOffline() {
            return Message.MessageBuilder.builder()
                    .withMessage(PLAYER_OFFLINE)
                    .withMessageType(MessageType.WARNING)
                    .build();
        }

        public static Message partyCreated(Faction faction) {
            String msg = PARTY_CREATED.replaceAll("%faction%", faction.getTag());
            return Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.INFO)
                    .build();
        }

        public static Message playerJoinedParty(Player player) {
            String msg = PLAYER_JOINED_PARTY.replaceAll("%player%", player.getName());

            return Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.SUCCESS)
                    .build();
        }

        public static Message youRemovedPlayerFromParty(Player player) {
            String msg = YOU_REMOVED_PLAYER_FROM_PARTY.replaceAll("%player%", player.getName());

            return Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.SUCCESS)
                    .build();
        }

        public static Message youWereKickedFromPartyBy(Player partyLeader) {
            String msg = YOU_WERE_KICKED_FROM_PARTY_BY.replaceAll("%player%", partyLeader.getName());

            return Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.WARNING)
                    .build();
        }

    }

}
