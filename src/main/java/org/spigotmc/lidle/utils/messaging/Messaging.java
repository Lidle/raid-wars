package org.spigotmc.lidle.utils.messaging;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.List;

/*Authored by: Matt Bessler 8/11/2018*/
class Messaging {

    static void sendMessage(CommandSender sender, String msg){
        sender.sendMessage(msg);
    }

    static void sendTitle(Player player, String title, String subTitle, int fadeIn, int stay, int fadeOut) {
        player.sendTitle(title, subTitle, fadeIn, stay, fadeOut);
    }

    static void broadcastMessage(String msg) {
        Bukkit.broadcastMessage(msg);
    }

    static String formatMessageToString(List<String> msg) {
        StringBuilder builder = new StringBuilder();

        for (int ind = 0; ind < msg.size(); ind++) {
            builder.append(msg.get(ind))
                    .append(ind == msg.size() - 1 ? "" : " ");
        }

        String colorizedMsg = ChatColor.translateAlternateColorCodes('&', builder.toString());
        return colorizedMsg;
    }

}
