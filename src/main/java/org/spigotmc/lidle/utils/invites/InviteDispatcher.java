package org.spigotmc.lidle.utils.invites;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.spigotmc.lidle.raidwars.invites.InviteType;
import org.spigotmc.lidle.utils.BukkitUtil;
import org.spigotmc.lidle.utils.generics.InviteActionDispatcher;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class InviteDispatcher implements InviteActionDispatcher {

    private AbstractInvite invite;
    private Inviteable invited;

    /**
     * @param invite the invite the {@link Inviteable} target received
     * @param invited the entity which the {@link AbstractInvite} was sent to
     * */
    public InviteDispatcher(AbstractInvite invite, Inviteable invited) {
        this.invite = invite;
        this.invited = invited;
    }

    /**
     * Dispatches a notification to the appropriate objects after the invite was accepted
     * */
    @Override
    public void dispatchInviteAccepted() {
        Message genericAcceptedNotification = Message.MessageBuilder.builder()
                .withMessage(Messages.GENERIC_ACCEPTED_INVITE)
                .withMessageType(MessageType.SUCCESS)
                .build();

        dispatchInviteAccepted(genericAcceptedNotification);
    }

    /**
     * Dispatches a notification to the appropriate objects after the invite was accepted
     * @param notification a message to send the object who invited the target that their invite was accepted
     * */
    public void dispatchInviteAccepted(Message notification) {
        //Accepting the invited object's invite
        invite.accept();
        invited.acceptInvite();

        //Notifying the object who invited that their invite has been accepted
        Inviteable whoInvited = invite.getWhoInvited();
        whoInvited.sendMessage(notification);

        if(invite.getInviteType() == InviteType.GAME) {
            //Notifying the object that accepted the invite
            String msg = Messages.YOUR_PARTY_ACCEPTED_GAME_INVITE;
            msg = msg.replaceAll("%faction%", whoInvited.getName());
            msg = msg.replaceAll("%player%", whoInvited.getName());

            Message message = Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.SUCCESS)
                    .build();

            invited.sendMessage(message);
        }
    }

    /**
     * Dispatches a notification to the appropriate objects after the invite has been denied
     * */
    @Override
    public void dispatchInviteDenied() {
        Message genericDeniedNotification = Message.MessageBuilder.builder()
                .withMessage(Messages.GENERIC_DENIED_INVITE)
                .withMessageType(MessageType.SUCCESS)
                .build();

        dispatchInviteDenied(genericDeniedNotification);
    }

    /**
     * Dispatches a notification to the appropriate objects after the invite has been denied
     * @param notification a message to send the object who invited the target that their invite was denied
     * */
    public void dispatchInviteDenied(Message notification) {
        //Denying the invited object's invite
        invite.deny();
        invited.denyInvite();

        //Notifying the object who invited that their invite has been denied
        Inviteable whoInvited = invite.getWhoInvited();
        whoInvited.sendMessage(notification);

        if(invite.getInviteType() == InviteType.GAME) {
            //Notifying the object that accepted the invite
            String msg = Messages.YOUR_PARTY_DENIED_GAME_INVITE;
            msg = msg.replaceAll("%faction%", whoInvited.getName());

            Message message = Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.WARNING)
                    .build();

            invited.sendMessage(message);
        }
    }

    /**
     * Dispatches an invite to the invited object & notifies them
     * */
    @Override
    public void dispatchInviteRequest() {
        String msg = Messages.YOUR_PARTY_WAS_INVITED_TO_GAME;
        String replace = BukkitUtil.coloredMessage(ChatColor.RED, invite.getName());
        msg = msg.replaceAll("%faction%", replace);
        msg = msg.replaceAll("%player%", replace);
        Message invitedReceivedNotification = Message.MessageBuilder.builder()
                .withMessage(msg)
                .withMessageType(MessageType.SUCCESS)
                .build();

        dispatchInviteRequest(invitedReceivedNotification);
    }

    /**
     * Dispatches an invite to the invited object & notifies them
     * @param notification a message to send the invited target that they have been invited
     * */
    public void dispatchInviteRequest(Message notification) {
        invited.invite(invite);
        invited.sendMessage(notification);
    }
}
