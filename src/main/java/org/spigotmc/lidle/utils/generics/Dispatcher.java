package org.spigotmc.lidle.utils.generics;

public interface Dispatcher {

    /**
     * Facilitates communication between two or more objects
     * */
    void dispatch();

}
