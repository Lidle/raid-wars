package org.spigotmc.lidle.utils.generics;

public interface Named {

    /**
     * The name of the object
     * */
    String getName();

}
