package org.spigotmc.lidle.utils.generics;

import org.spigotmc.lidle.utils.invites.AbstractInvite;
import org.spigotmc.lidle.utils.invites.Inviteable;

public interface InviteActionDispatcher extends Dispatcher {

    /**
     * Accepts the invite and notifies both parties
     * */
    void dispatchInviteAccepted();

    /**
     * Denies the invite and notifies both parties
     * */
    void dispatchInviteDenied();

    /**
     * Dispatches an invite to the appropriate objects
     * */
    void dispatchInviteRequest();

    default void dispatch() {
        dispatch();
    }

}
