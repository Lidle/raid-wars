package org.spigotmc.lidle.utils.event;

import org.bukkit.event.Listener;
import org.spigotmc.lidle.RaidWars;
import org.spigotmc.lidle.events.subevents.savagefactions.FactionsEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EventListenerRegisterer {

    private static final RaidWars pl = RaidWars.inst();

    private static List<Listener> pluginEvents = new ArrayList<>();

    /**
     * Registers all {@link Listener} that are added to this class.
     * Use {@link #addListener(Listener) addListener} or
     * {@link #addListeners(Collection) addListeners} to register events
     * in this class
     * */
    public static void registerAllEvents(){
        initEventHandlers();

        pluginEvents.forEach(listener -> {
            pl.getServer().getPluginManager().registerEvents(listener, pl);
        });
    }

    private static void initEventHandlers(){
        FactionsEvent.registerListeners();
    }

    public static void addListener(Listener eventListener){
        pluginEvents.add(eventListener);
    }

    public static void addListeners(Collection<? extends Listener> eventListeners){
        pluginEvents.addAll(eventListeners);
    }

}
