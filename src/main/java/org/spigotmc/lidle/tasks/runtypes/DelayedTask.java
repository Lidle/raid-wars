package org.spigotmc.lidle.tasks.runtypes;

public class DelayedTask implements Delay {

    private long delay;

    @Override
    public void setDelay(long delay) {
        this.delay = delay;
    }

    @Override
    public long getDelay() {
        return delay;
    }
}
