package org.spigotmc.lidle.tasks.runtypes;

public class ImmediateTask implements Immediate {

    private long period = 0L;

    @Override
    public void setPeriod(long period) {
        this.period = period;
    }

    @Override
    public long getPeriod() {
        return period;
    }
}
