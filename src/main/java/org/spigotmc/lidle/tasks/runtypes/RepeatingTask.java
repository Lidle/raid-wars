package org.spigotmc.lidle.tasks.runtypes;

public class RepeatingTask implements Repeat {

    private long period;

    @Override
    public void setPeriod(long period) {
        this.period = period;
    }

    @Override
    public long getPeriod() {
        return period;
    }
}
