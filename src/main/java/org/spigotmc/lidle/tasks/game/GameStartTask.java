package org.spigotmc.lidle.tasks.game;

import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.tasks.TaskExecutor;

import java.util.Set;

public class GameStartTask implements TaskExecutor {

    private GameExecutor executor;
    private GameStartRunnable runnable;

    public GameStartTask(Game game, long countdown) {
        executor = new GameExecutor(countdown);
        runnable = new GameStartRunnable(game, countdown);
    }

    @Override
    public void executeTask() {
        executor.execute(runnable);
    }
}
